package models

import (
	"github.com/jmoiron/sqlx"
	V "gopkg.in/validator.v2"

	"fm-libs/orm"
	"fm-libs/verr"

	"notification/conf"
	"notification/db"

	"errors"
	"fmt"
)

var (
	ORM *orm.Orm
	cf  *conf.AppConf
	DB  *sqlx.DB

	//Model errors
	ErrNotFound     = errors.New("items not found")
	ErrPageNotFound = errors.New("page not found")
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: ", err)
	}

	return nil
}

func Init() error {
	var err error

	if err := initConf(); err != nil {
		return err
	}

	DB, err = db.GetDb()
	if err != nil {
		return err
	}

	ORM, err = orm.New(DB, cf.SqlDir)
	if err != nil {
		return fmt.Errorf("orm initialize error %s", err)
	}

	V.SetValidationFunc("dic", verr.DicValid)
	V.SetValidationFunc("uuid", verr.UUIDValid)

	return nil
}
