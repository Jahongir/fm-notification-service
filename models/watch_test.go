package models

import (
	"fmt"
	"testing"

	"fm-libs/orm"
)

var (
	WatchId     string
	WatchEntity = "work"
)

func GetByMaskDepsCreate() error {
	watches := []Watch{
		Watch{
			Entity:  "issue",
			UserId:  UID,
			Actions: ActionCreate,
			Std:     orm.Std{CreatedBy: UID},
		},
		Watch{
			Entity:  "issue",
			UserId:  UID,
			Actions: ActionCreate + ActionUpdate + ActionDelete,
			Std:     orm.Std{CreatedBy: UID},
		},
		Watch{
			Entity:  "service",
			UserId:  UID,
			Actions: ActionCreate + ActionUpdate,
			Std:     orm.Std{CreatedBy: UID},
		},
		Watch{
			Entity:  "service",
			UserId:  UID,
			Actions: ActionCreate + ActionUpdate + ActionDelete + ActionRestore,
			Std:     orm.Std{CreatedBy: UID},
		},
	}

	for _, watch := range watches {
		if err := watch.Create(schema); err != nil {
			return fmt.Errorf("watch create error: %s", err)
		}
	}

	return nil
}

func TestWatchCreate(t *testing.T) {
	model := Watch{}
	model.Entity = "issue"
	model.UserId = UID
	model.Actions = 3
	model.CreatedBy = UID

	t.Logf("model: %#v \n", schema)
	if err := model.Create(schema); err != nil {
		t.Error(err)
	}

	WatchId = model.Id
}

func TestWatchUpdate(t *testing.T) {
	model := Watch{}
	model.Id = WatchId
	model.Entity = WatchEntity
	model.UserId = UID
	model.Actions = 3
	model.UpdatedBy.String = UID
	model.UpdatedBy.Valid = true

	if err := model.Update(schema); err != nil {
		t.Error("update error: ", err)
	}
}

func TestWatchGet(t *testing.T) {
	model := Watch{}
	if err := model.Get(schema, WatchId); err != nil {
		t.Errorf("model: %+v ,\n err: %v", model, err)
		return
	}

	t.Logf("model: %#v \n", model)

	if model.Entity != WatchEntity {
		t.Errorf("expected Entity: %s, got: %s \n", WatchEntity, model.Entity)
	}

}

func TestWatchGetByUser(t *testing.T) {
	model := Watch{}
	dests, pages, err := model.GetByUser(schema, UID, []string{""}, 0, 0)
	if err != nil {
		t.Error(err)
	}
	if len(dests) < 1 {
		t.Error("dests length should be grater than 0")
		return
	}

	if pages < 1 {
		t.Error("pages should be grater than 0")
		return
	}
}

func TestWatchDelete(t *testing.T) {
	model := Watch{}
	model.Id = WatchId
	model.DeletedBy.String = UID
	model.DeletedBy.Valid = true

	if err := model.Delete(schema); err != nil {
		t.Error(err)
	}
}

func TestWatchRestore(t *testing.T) {
	model := Watch{}
	model.Id = WatchId
	if err := model.Restore(schema); err != nil {
		t.Error(err)
	}
}

func TestConvertActions(t *testing.T) {
	model := Watch{}

	asserts := []struct {
		actions     []string
		result      int
		errorString string
	}{
		{[]string{"deleted", "updated", "restored", "created"}, 15, ""},
		{[]string{"created", "updated", "deleted"}, 7, ""},
		{[]string{"created", "updated"}, 3, ""},
		{[]string{"created", "???"}, 1, fmt.Sprintf("action not found: %s", "???")},
	}

	for _, assert := range asserts {
		result, err := model.ConvertActions(assert.actions)
		if result != assert.result || (err != nil && err.Error() != assert.errorString) {
			t.Errorf("expected result: %d got: %d \n", result, assert.result)
			t.Errorf("expected error: %s got: %s \n", err, assert.errorString)
		}
	}
}

func TestGetAllByMask(t *testing.T) {
	if err := GetByMaskDepsCreate(); err != nil {
		t.Error(err)
		return
	}

	model := Watch{}

	asserts := []struct {
		entity        string
		action        string
		result_length int
	}{
		{"issue", "created", 2},
		{"issue", "updated", 1},
		{"service", "updated", 2},
		{"service", "restored", 1},
	}

	for _, assert := range asserts {
		result, err := model.GetByMask(schema, assert.entity, assert.action)
		if err != nil {
			t.Error("getByMask error: ", err)
			continue
		}
		if len(result) != assert.result_length {
			t.Errorf("expected res_length: %d got: %d \n", assert.result_length, len(result))
		}
	}
}

func TestWatchInitialInsert(t *testing.T) {
	model := Watch{
		UserId:  UID,
		Actions: ActionCreate + ActionUpdate + ActionDelete + ActionRestore,
		Std:     orm.Std{CreatedBy: UID},
	}

	if err := model.InitialInsert(schema); err != nil {
		t.Error("initial insert error: ", err)
		return
	}
}
