package db

import (
	"notification/conf"

	"testing"
)

func init() {
	conf.ConfDir = "../conf"
}

func TestGetDb(t *testing.T) {
	if _, err := GetDb(); err != nil {
		t.Error("Get db error: ", err)
	}
}
