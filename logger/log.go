package logger

import (
	"github.com/Sirupsen/logrus"

	"notification/conf"
	"notification/rabbit"

	"fm-libs/log"

	"fmt"
)

var (
	cf *conf.AppConf
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: %s", err)
	}

	return nil
}

func Init() error {
	if err := initConf(); err != nil {
		return err
	}

	//in production mode log will send logs to elastic search
	rmq, err := rabbit.GetRabbit()
	if err != nil {
		return fmt.Errorf("get rabbit error: ", err)
	}

	if cf.LogToRabbit {
		log.LogToRabbit(rmq, conf.ServiceName)
	}

	if cf.LogDir != "" {
		log.LogToFile(cf.LogDir)
	}

	lvl, err := logrus.ParseLevel(cf.LogLvl)
	if err != nil {
		return err
	}

	log.SetLevel(lvl)

	return nil
}
