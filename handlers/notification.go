package handlers

import (
	"github.com/pborman/uuid"

	"fm-libs/api"
	"fm-libs/config"
	"fm-libs/handler"
	m "notification/models"

	"fmt"
)

type (
	Notification struct{}

	NotificationJson struct {
		NtfIds []string `json:"notification_ids"`
		Paging
		handler.BaseRpcReq
	}
)

func (t *Notification) GetByUser(jsonData []byte, resp *[]byte) error {
	var req NotificationJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessUserIdKey},
		func(p *handler.Params) api.Response {
			var model m.Notification

			res, pages, err := model.GetByUser(p.SessData[config.SessUserIdKey], req.Sort, req.Page, req.PageSize)
			if err == m.ErrNotFound {
				return api.Response{ErrorCode: api.ErrCodeNotFound, ErrorMessage: "item not found in database"}
			}

			if err == m.ErrPageNotFound {
				return api.Response{ErrorCode: api.ErrCodePageNotFound, ErrorMessage: "page not found"}
			}

			if err != nil {
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			return api.Response{
				ErrorCode: api.ErrCodeSuccess,
				Data:      res,
				Paging: api.Paging{
					Page:     req.Page,
					PageSize: req.PageSize,
					Max:      pages,
				},
			}
		})
	return nil
}

func (t *Notification) SetView(jsonData []byte, resp *[]byte) error {
	var req NotificationJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessUserIdKey},
		func(p *handler.Params) api.Response {
			var model m.Notification
			model.Viewed = true
			model.UpdatedBy.String = p.SessData[config.SessUserIdKey]
			model.UpdatedBy.Valid = true

			//checking for notificatoins ids validness
			for _, ntf_id := range req.NtfIds {
				if ntf_uuid := uuid.Parse(ntf_id); ntf_uuid == nil {
					return api.Response{
						ErrorCode:    api.ErrCodeValidation,
						ErrorMessage: fmt.Sprintf("invalid uuid: %s", ntf_id),
					}
				}
			}

			if err := model.Update(req.NtfIds); err != nil {
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			return api.Response{ErrorCode: api.ErrCodeSuccess, Data: model}
		})
	return nil
}
