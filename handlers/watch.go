package handlers

import (
	"notification/conf"
	m "notification/models"

	"fm-libs/api"
	"fm-libs/config"
	"fm-libs/handler"
	pu "fm-libs/preputils"
	"fm-libs/verr"

	sq "database/sql"
)

type (
	Watch struct{}

	WatchJson struct {
		Id      string   `json:"id,omitempty" get:"uuid,nonzero" update:"uuid,nonzero" delete:"uuid,nonzero" restore:"uuid,nonzero"`
		Entity  string   `json:"entity" create:"nonzero" update:"nonzero"`
		Actions []string `json:"actions" create:"nonzero" update:"nonzero"`
		UserId  string   `json:"user_id"`

		Paging
		handler.BaseRpcReq
	}
)

func (t *Watch) Create(jsonData []byte, resp *[]byte) error {
	var req WatchJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessUserIdKey},
		func(p *handler.Params) api.Response {
			var model m.Watch
			model.CreatedBy = p.SessData[config.SessUserIdKey]
			model.Entity = req.Entity
			model.UserId = p.SessData[config.SessUserIdKey]

			if resp, ok := verr.Validate(req, "create"); !ok {
				return resp
			}

			actions, err := model.ConvertActions(req.Actions)
			if err != nil {
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			model.Actions = actions

			if err := model.Create(p.Schema); err != nil {
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			pu.SendJsonEmit(Rmq, conf.ServiceName+".watch.created", pu.EventData{
				Data:     model,
				SessData: p.SessData,
			})

			return api.Response{ErrorCode: api.ErrCodeSuccess, Data: model}
		})
	return nil
}

func (t *Watch) Get(jsonData []byte, resp *[]byte) error {
	var req WatchJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{},
		func(p *handler.Params) api.Response {
			var model m.Watch

			if resp, ok := verr.Validate(req, "get"); !ok {
				return resp
			}

			if err := model.Get(p.Schema, req.Id); err != nil {
				if err == sq.ErrNoRows {
					return api.Response{ErrorCode: api.ErrCodeNotFound, ErrorMessage: err.Error()}
				}
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}
			return api.Response{ErrorCode: api.ErrCodeSuccess, Data: model}
		})
	return nil
}

func (t *Watch) Update(jsonData []byte, resp *[]byte) error {
	var req WatchJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessUserIdKey},
		func(p *handler.Params) api.Response {
			var model m.Watch
			model.Id = req.Id
			model.Entity = req.Entity
			model.UserId = p.SessData[config.SessUserIdKey]
			model.UpdatedBy.String = p.SessData[config.SessUserIdKey]
			model.UpdatedBy.Valid = true

			if resp, ok := verr.Validate(req, "update"); !ok {
				return resp
			}

			actions, err := model.ConvertActions(req.Actions)
			if err != nil {
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			model.Actions = actions

			if err := model.Update(p.Schema); err != nil {
				if err == sq.ErrNoRows {
					return api.Response{ErrorCode: api.ErrCodeNotFound, ErrorMessage: err.Error()}
				}
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			pu.SendJsonEmit(Rmq, conf.ServiceName+".watch.updated", pu.EventData{
				Data:     model,
				SessData: p.SessData,
			})

			return api.Response{ErrorCode: api.ErrCodeSuccess, Data: model}
		})
	return nil
}

func (t *Watch) Delete(jsonData []byte, resp *[]byte) error {
	var req WatchJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessUserIdKey},
		func(p *handler.Params) api.Response {
			var model m.Watch
			model.Id = req.Id
			model.DeletedBy.String = p.SessData[config.SessUserIdKey]
			model.DeletedBy.Valid = true

			if resp, ok := verr.Validate(req, "delete"); !ok {
				return resp
			}

			if err := model.Delete(p.Schema); err != nil {
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			pu.SendJsonEmit(Rmq, conf.ServiceName+".watch.deleted", pu.EventData{
				Data:     req,
				SessData: p.SessData,
			})

			return api.Response{ErrorCode: api.ErrCodeSuccess}
		})

	return nil
}

func (t *Watch) Restore(jsonData []byte, resp *[]byte) error {
	var req WatchJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{},
		func(p *handler.Params) api.Response {
			var model m.Watch
			model.Id = req.Id

			if resp, ok := verr.Validate(req, "restore"); !ok {
				return resp
			}

			if err := model.Restore(p.Schema); err != nil {
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			pu.SendJsonEmit(Rmq, conf.ServiceName+".watch.restored", pu.EventData{
				Data:     req,
				SessData: p.SessData,
			})

			return api.Response{ErrorCode: api.ErrCodeSuccess}
		})

	return nil
}

func (t *Watch) GetByUser(jsonData []byte, resp *[]byte) error {
	var req WatchJson

	*resp = handler.RpcHandle(
		jsonData,
		&req,
		[]string{config.SessUserIdKey},
		func(p *handler.Params) api.Response {
			var model m.Watch

			if resp, ok := verr.Validate(req, "getbyuser"); !ok {
				return resp
			}

			res, pages, err := model.GetByUser(
				p.Schema,
				p.SessData[config.SessUserIdKey],
				req.Sort,
				req.Page,
				req.PageSize,
			)
			if err == m.ErrNotFound {
				return api.Response{ErrorCode: api.ErrCodeNotFound, ErrorMessage: "item not found in database"}
			}

			if err == m.ErrPageNotFound {
				return api.Response{ErrorCode: api.ErrCodePageNotFound, ErrorMessage: "page not found"}
			}

			if err != nil {
				return api.Response{ErrorCode: api.ErrCodeModelCall, ErrorMessage: err.Error()}
			}

			return api.Response{
				ErrorCode: api.ErrCodeSuccess,
				Data:      res,
				Paging: api.Paging{
					Page:     req.Page,
					PageSize: req.PageSize,
					Max:      pages,
				},
			}
		})
	return nil
}
