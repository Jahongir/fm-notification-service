package handlers

import (
	"github.com/streadway/amqp"
	V "gopkg.in/validator.v2"

	"notification/conf"
	"notification/logger"
	"notification/models"
	"notification/rabbit"
	"notification/redis"

	"fm-libs/handler"
	"fm-libs/session/redis"
	"fm-libs/verr"

	"fmt"
)

var (
	Rmq *amqp.Connection
	cf  *conf.AppConf
)

type (
	Paging struct {
		Sort     []string `json:"sort,omitempty"`
		Page     int      `json:"page,omitempty"`
		PageSize int      `json:"per_page,omitempty"`
	}
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: ", err)
	}

	return nil
}

func Init() error {
	var err error
	if err := initConf(); err != nil {
		return err
	}

	if err := models.Init(); err != nil {
		return fmt.Errorf("models init error: %s", err)
	}

	rds, err := redis.GetRedis()
	if err != nil {
		return fmt.Errorf("redis get error: %s", err)
	}

	handler.Session = &session.RedisStore{Conn: rds}

	V.SetValidationFunc("uuid", verr.UUIDValid)

	Rmq, err = rabbit.GetRabbit()
	if err != nil {
		return fmt.Errorf("rabbit get error: %s", err)
	}

	if err := logger.Init(); err != nil {
		return fmt.Errorf("logger init error: %s", err)
	}

	return nil
}
