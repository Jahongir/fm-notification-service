package conf

import "fm-libs/config"

var (
	ConfDir = "conf"
	RunMode = "dev"
	appConf *AppConf
)

const ServiceName = "notification"

type AppConf struct {
	Db    config.Db
	Redis config.Redis
	Rmq   config.Rmq

	Addr   string
	SqlDir string

	MigrateTable string

	RoutingKeys struct {
		FleetCreate   string
		ContactCreate string
	}

	// logging level
	LogLvl string
	// logging directory name
	LogDir string
	// Log to rabbit
	LogToRabbit bool

	MigrateDir string

	// Application version
	Version string
	// Dependencies
	Deps map[string]string
}

func (c *AppConf) SetDefaults() {
	c.Db.SetDefaults()
	c.Redis.SetDefaults()
	c.Rmq.Url = "amqp://localhost:5672"

	c.Addr = ":3900"
	c.SqlDir = "sql"

	c.MigrateTable = "ntf_migrations"

	c.RoutingKeys.FleetCreate = "fleet.fleet.created"
	c.RoutingKeys.ContactCreate = "fleet.contact.created"

	c.LogLvl = "info"
	c.LogDir = ""
	c.LogToRabbit = true

	c.MigrateDir = "migrations"

	c.Version = "0.0.2"
	c.Deps = map[string]string{
		"fm-libs": "0.2.2",
	}
}

func GetConf() (*AppConf, error) {
	var err error

	//appConf not initialized initialize it
	if appConf == nil {
		cf := AppConf{}

		cf.SetDefaults()

		confFile := ConfDir + "/app." + RunMode + ".toml"
		if err = config.ReadResources(&cf, confFile, "env"); err != nil {
			return nil, err
		}

		appConf = &cf
	}

	return appConf, nil
}
