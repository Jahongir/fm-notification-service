package event

import (
	"fm-libs/util"

	"notification/conf"
	"notification/db"
	"notification/models"

	"log"
	"os"
	"testing"
)

const (
	FID  = "6fa82bb1-c9a8-4aa6-8a9e-cb4e26f84712"
	UUID = "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11"

	NOTIFICATION_FLEET_KEY   = "notification.fleet.created"
	NOTIFICATION_CONTACT_KEY = "notification.contact.created"
)

var (
	schema string
)

func TestMain(m *testing.M) {
	conf.ConfDir = "../conf"
	conf.RunMode = "test"

	cfg, err := conf.GetConf()
	if err != nil {
		log.Fatal("get conf error: ", err)
	}

	cfg.SqlDir = "../sql"

	if err := Init(); err != nil {
		log.Fatal("Init() error: ", err)
	}

	schema = util.FleetSchema(FID)

	db, err := db.GetDb()
	if err != nil {
		log.Fatal("get db error: ", err)
	}

	//Create fleet schema
	db.MustExec("CREATE SCHEMA IF NOT EXISTS " + schema)

	fleet := models.Fleet{}
	if err := fleet.Create(FID); err != nil {
		log.Fatal("get db error: ", err)
	}

	exitCode := m.Run()

	//Delete fleet schema
	db.MustExec("DROP SCHEMA IF EXISTS " + schema + " CASCADE;")

	os.Exit(exitCode)
}
