package event

import (
	"github.com/streadway/amqp"

	"fm-libs/config"
	pu "fm-libs/preputils"
	rbt "fm-libs/rabbit"

	"notification/models"

	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func CreateListenDeps() error {
	var model models.Watch
	model.Entity = "issue"
	model.UserId = UUID
	model.CreatedBy = UUID

	actions, err := model.ConvertActions([]string{"created"})
	if err != nil {
		return fmt.Errorf("convert actions error: %s", err)
	}

	model.Actions = actions

	if err := model.Create(schema); err != nil {
		fmt.Errorf("watch create error: %s", err)
	}

	return nil
}

//Emit function for creating fake fleet.created event
func Emit(routing_key string, data interface{}) error {
	ch, err := rmq.Channel()
	if err != nil {
		return fmt.Errorf("rabbit channel create error: %s", err)
	}

	json, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("marshal error: %s", err)
	}

	if err = ch.Publish(rbt.ExchangeEvents, routing_key, false, false, amqp.Publishing{
		ContentType:  "application/json",
		Body:         json,
		DeliveryMode: amqp.Transient,
	}); err != nil {
		return fmt.Errorf("rabbit publish error: %s", err)
	}
	return nil
}

func TestListenAll(t *testing.T) {
	if err := CreateListenDeps(); err != nil {
		t.Error(err)
		return
	}

	succChan, errChan := ListenAllChanges()
	if err := Emit(
		"issue.issue.created",
		pu.EventData{
			Data: map[string]string{
				"id":         FID,
				"created_by": UUID,
			},
			SessData: map[string]string{
				config.SessFleetKey:  FID,
				config.SessUserIdKey: UUID,
			},
		},
	); err != nil {
		t.Error("publish error ", err)
	}

	select {
	case err := <-errChan:
		if err != nil {
			t.Error("Listen error ", err)
		}
	case <-succChan:
	case <-time.After(10 * time.Second):
		t.Error("Event not emitted")
	}
}

func TestListenFleet(t *testing.T) {
	_, errChan := ListenFleetCreate()

	flCreateSuccChan := make(chan string, 10)

	rbt.Watch(rmq, "test-vehicle.fleet.create", NOTIFICATION_FLEET_KEY, rbt.ExchangeEvents, func(event amqp.Delivery) {
		err := event.Ack(false)
		if err == nil {
			flCreateSuccChan <- "occured"
		}
	})

	if err := Emit(
		cf.RoutingKeys.FleetCreate,
		pu.FleetEventData{
			Data: pu.Fleet{
				Id:        FID,
				CreatedBy: UUID,
			},
		},
	); err != nil {
		t.Error("publish error ", err)
	}

	select {
	case err := <-errChan:
		if err != nil {
			t.Error("Listen error ", err)
		}
	case <-flCreateSuccChan:
	case <-time.After(5 * time.Second):
		t.Error("Event not emitted")
	}

}

func TestLitenContactCreate(t *testing.T) {
	_, errChan := ListenContactCreate()

	flCreateSuccChan := make(chan string, 10)

	rbt.Watch(rmq, "test-notification.fleet.create", NOTIFICATION_CONTACT_KEY, rbt.ExchangeEvents, func(event amqp.Delivery) {
		err := event.Ack(false)
		if err == nil {
			flCreateSuccChan <- "occured"
		}
	})

	if err := Emit(
		cf.RoutingKeys.ContactCreate,
		ContactEvent{
			Data: Contact{
				UserId: UUID,
			},
			EventData: pu.EventData{
				SessData: map[string]string{
					config.SessFleetKey: FID,
				},
			},
		},
	); err != nil {
		t.Error("publish error ", err)
	}

	select {
	case err := <-errChan:
		if err != nil {
			t.Error("Listen error ", err)
		}
	case <-flCreateSuccChan:
	case <-time.After(5 * time.Second):
		t.Error("Event not emitted")
	}

}
