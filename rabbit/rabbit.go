package rabbit

import (
	"github.com/streadway/amqp"

	"fm-libs/rabbit"
	"notification/conf"

	"encoding/json"
	"fmt"
)

var (
	cf  *conf.AppConf
	rmq *amqp.Connection
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: ", err)
	}

	return nil
}

func initRabbit() error {
	var err error

	if err := initConf(); err != nil {
		return err
	}

	rmq_url := cf.Rmq.Url
	//creating base exchanges in fleet-management
	rmq, err = rabbit.CreateExchanges(rmq_url)
	if err != nil {
		return fmt.Errorf("rabbit create exchanges error:%s url:%s", err, rmq_url)
	}

	return nil
}

func GetRabbit() (*amqp.Connection, error) {
	//If rabbit initialized already return it
	if rmq != nil {
		return rmq, nil
	}

	//initializing rabbit
	if err := initRabbit(); err != nil {
		return nil, err
	}

	return rmq, nil
}

func Emit(key string, data interface{}) error {
	ch, err := rmq.Channel()
	if err != nil {
		return fmt.Errorf("rabbit channel create error: %s", err)
	}

	json, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("marshal error: %s", err)
	}

	routing_key := conf.ServiceName + "." + key

	if err = ch.Publish(rabbit.ExchangeEvents, routing_key, false, false, amqp.Publishing{
		ContentType:  "application/json",
		Body:         json,
		DeliveryMode: amqp.Transient,
	}); err != nil {
		return fmt.Errorf("rabbit publish error: %s", err)
	}

	return nil
}
