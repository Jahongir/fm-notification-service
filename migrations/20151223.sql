-- +migrate Up
CREATE TABLE IF NOT EXISTS ntf_watches (
    id UUID UNIQUE PRIMARY KEY NOT NULL,
    entity TEXT NOT NULL,
    actions INTEGER NOT NULL DEFAULT 1,
    user_id UUID NOT NULL,

    created_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    deleted_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,

    created_by UUID NOT NULL,
    updated_by UUID,
    deleted_by UUID,

    deleted BOOLEAN DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS ntf_notifications (
    id UUID UNIQUE PRIMARY KEY NOT NULL,
    content JSON NOT NULL,
    viewed BOOLEAN NOT NULL DEFAULT FALSE,
    user_id UUID NOT NULL,
    fleet_id UUID NOT NULL,
    entity TEXT NOT NULL,
    action TEXT NOT NULL,

    created_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    deleted_at TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,

    created_by UUID NOT NULL,
    updated_by UUID,
    deleted_by UUID,

    deleted BOOLEAN DEFAULT FALSE
);


-- +migrate Down
DROP TABLE IF EXISTS ntf_watches CASCADE;
DROP TABLE IF EXISTS ntf_notifications CASCADE;
