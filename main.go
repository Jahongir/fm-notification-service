package main

import (
	"github.com/fatih/color"
	"github.com/jmoiron/sqlx"

	"fm-libs/dbutil"
	"fm-libs/log"
	"fm-libs/verr"

	"notification/conf"
	DB "notification/db"
	"notification/event"
	"notification/handlers"
	"notification/logger"
	"notification/models"

	"fmt"
)

var (
	db *sqlx.DB
	cf *conf.AppConf //config
)

func Init() error {
	var err error

	err = logger.Init()
	if err != nil {
		return fmt.Errorf("init log error: %s", err)
	}

	cf, err = conf.GetConf()
	if err != nil {
		return fmt.Errorf("get conf error: %s", err)
	}

	db, err = DB.GetDb()
	if err != nil {
		return fmt.Errorf("get db error: %s", err)
	}

	verr.DB = db

	//Initializing all packages
	if err := models.Init(); err != nil {
		return fmt.Errorf("models init error: %s", err)
	}

	if err := handlers.Init(); err != nil {
		return fmt.Errorf("handlers init error: %s", err)
	}

	if err := event.Init(); err != nil {
		return fmt.Errorf("event init error: %s", err)
	}

	return nil
}

func PrintVersion() {
	color.Blue("Version: %s", cf.Version)
	color.Green("Dependencies:")
	for key, value := range cf.Deps {
		color.Magenta("\t%s: %s\n", key, value)
	}
}

func main() {
	if err := Init(); err != nil {
		log.Log("main init", log.ErrorLevel, log.M{"error": err.Error()})
	}

	// Printing version of app and it's dependencies
	PrintVersion()

	//running migrations
	n, err := dbutil.MigrateUp(db.DB, "postgres", cf.MigrateTable)
	if err != nil {
		log.Log("migrate up", log.FatalLevel, log.M{"error": err.Error(), "table": cf.MigrateTable})
	}

	succChan, errChan := event.ListenAllChanges()
	go func() {
		for info := range succChan {
			log.Log("event", log.InfoLevel, log.M{
				"message": info,
			})
		}
	}()

	go func() {
		for err := range errChan {
			log.Log("event", log.ErrorLevel, log.M{
				"error": err.Error(),
			})
		}
	}()

	event.ListenFleetCreate()
	event.ListenContactCreate()

	log.Log("applied migrations", log.InfoLevel, log.M{
		"migrations": n,
	})

	log.Log("notification started", log.InfoLevel, log.M{
		"at":   cf.Addr,
		"mode": conf.RunMode,
	})

	handlers.Listen()
}
